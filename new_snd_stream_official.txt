; ==========================================
; SOUND STREAM: Official Version
; Plays a streamed sound as the main BGM
; BGM IDs 1000+X are translated as playing
; file "SOUND/BGM/streamed_X.wav"
; ==========================================

.nds

.definelabel RESERVE_CHANNEL, 16 ; Set to 15 to actually reserve the channel for snd_stream (Note this may alter the quality of sequenced BGMs)

.definelabel FStreamAlloc,    0x2008168     ;() Is usually done first, before any reading is done. Seems to instantiate the Filestream?
.definelabel FStreamCtor,     0x2008204     ;(r0 = PtrFStreamStruct)  Zeroes the content of the struct
.definelabel FStreamFOpen,    0x2008210     ;(r0 = PtrFStreamStruct, r1 = PtrFPath) Open the file for reading
.definelabel FStreamSeek,     0x20082A8     ;(r0 = PtrFStreamStruct, r1 = OffsetToSeekTo, r2 = unknown?(usually 0) )
;2008244h
.definelabel FStreamRead,     0x2008254     ;(r0 = PtrFStreamStruct, r1 = PtrOutBuffer, r2 = NbBytesToRead ) Read the ammount of bytes specified to the buffer, for the FStream object
.definelabel FStreamClose,    0x20082C4     ;(r0 = PtrFStreamStruct)  Close the filestream
.definelabel FStreamDealloc,  0x2008194     ;() ???

.definelabel EuclidianDivision, 0x0208FEA4 ; 0x0209023C In EU
; The only change that requires an address update for the EU version (A diff with the previous version should give you the other changes)

.definelabel snd_addr, 0x023B0000
.definelabel swap_time, 0x61

.open "arm9.bin", 0x02000000
	.org 0x020198D0
	.area 0x4
		b HookCheckBGM ; -> Hook Check BGM: r6: ID, r5: Fade In, r4: Volume
	EndHookCheckBGM:
	.endarea
	.org 0x0201996C
	.area 0x4
		b HookStartBGM ; -> Hook Play BGM: r6: ID, r5: Fade In, r4: Volume
	.endarea
	.org 0x02019B30
	.area 0x4
		b HookStopBGM ; -> Hook Stop BGM: r0/r4: Fade Out
	EndHookStopBGM:
	.endarea
	
	.org 0x02019C64
	.area 0x4
		b HookChangeBGM ; -> Hook Change BGM: r5: Duration, r4: Volume
	EndHookChangeBGM:
	.endarea
	
	.org 0x020743EC
	.area 0x4
		mov r2,RESERVE_CHANNEL
	.endarea
	.org 0x020743F4
	.area 0x4
		cmp r0,RESERVE_CHANNEL
	.endarea

	.org 0x020A6A58
	.area 0x800
	playing:        ; TIP: set/unset this to play/stop the sound file. Available since v1.
		.dcb 0
	ofstream:       ; 
		.dcb 0
	reload:         ; TIP: set this to reload the sound file. Available since v1.
		.dcb 0
		.dcb 0
	start:          ; 
		.word 0x0
	timer:          ; 
		.word 0x0
	filename:       ; TIP: this controls the file to use. Available since v1.
		.fill 0x40, 0x0
	fstream:        ; 
		.fill 0x48, 0x0
	loop:           ; TIP: this controls where the loop point of the sound will be; measured in bytes. Available since v1.
		.word 0x0
	volume:         ; TIP: this controls the volume of the current sound (/127); currently at 60/127. Available since this version.
		.word 0x3C
	fade_to:        ; 
		.word 0x0
	fade_time:      ; 
		.word 0x0
	fade_play:      ; 
		.word 0x0
	freq:           ; 
		.word 0x0
	data_start:     ;
		.word 0x0
	data_end:       ;
		.word 0x0
	HookCheckBGM:
		bl 0x02002CB4
		ldr r0,[bgm_no]
		cmp r6,r0
		beq EndHookCheckBGM
		ldr r1,=playing
		mov r0,#0
		strb r0,[r1]
		mvn r0,#0
		str r0,[bgm_no]
		b EndHookCheckBGM
	HookStartBGM:
		ldr r0,[bgm_no]
		cmp r6,r0
		beq 0x02019AF8
		ldr r0,=filename
		ldr r1,=string_strm
		sub r2,r6,#0x3E8
		bl 0x0200D634 ; SPrintF
		str r6,[bgm_no]
		rsb  r1,r4,r4,lsl #0x7
		mov  r0,r1,asr #0x8
		str r0,[fade_to]
		mov  r0,#0x3E8
		mul  r0,r5,r0
		mov  r1,#0x258
		bl EuclidianDivision
		cmp r0,#0
		moveq r0,#1
		str r0,[fade_time]
		mov r0,#0
		str r0,[volume]
		ldr r1,=playing
		mov r0,#1
		str r0,[fade_play]
		strb r0,[r1]
		ldr r1,=reload
		strb r0,[r1]
		b 0x02019AF8
	HookStopBGM:
		mov  r4,r0
		mvn r0,#0
		str r0,[bgm_no]
		mov  r0,#0x3E8
		mul  r0,r4,r0
		mov  r1,#0x258
		bl EuclidianDivision
		cmp r0,#0
		moveq r0,#1
		str r0,[fade_time]
		mov r0,#0
		str r0,[fade_play]
		str r0,[fade_to]
		b EndHookStopBGM
	HookChangeBGM:
		bl 0x02002CB4
		rsb  r1,r4,r4,lsl #0x7
		mov  r0,r1,asr #0x8
		str r0,[fade_to]
		mov  r0,#0x3E8
		mul  r0,r5,r0
		mov  r1,#0x258
		bl EuclidianDivision
		cmp r0,#0
		moveq r0,#1
		str r0,[fade_time]
		b EndHookChangeBGM
	bgm_no:
		.word 0xFFFFFFFF
	ReadWAVChunk:
		stmdb r13!,{r14}
		ldr r0,=fstream
		ldr r1,[current_pos]
		mov r2,#0
		bl FStreamSeek
		
		ldr r0,=fstream
		ldr r1,=chunk_info
		mov r2,#8
		bl FStreamRead
		ldr r0,[chunk_info]
		ldr r1,=0x20746D66
		cmp r0,r1
		beq process_fmt
		ldr r1,=0x736D706C
		cmp r0,r1
		beq process_smpl
		ldr r1,=0x61746164
		cmp r0,r1
		beq process_data
		b ignore_chunk
	process_fmt:
		ldr r0,=fstream
		mov r1,#4
		mov r2,#1
		bl FStreamSeek
		ldr r0,=fstream
		ldr r1,=freq
		mov r2,#4
		bl FStreamRead
		b ignore_chunk
	process_smpl:
		ldr r0,=fstream
		mov r1,#0x1C
		mov r2,#1
		bl FStreamSeek
		ldr r0,=fstream
		ldr r1,=chunk_info ; Replace chunk info
		mov r2,#4
		bl FStreamRead
		ldr r1,[chunk_info]
		cmp r1,#0
		beq ignore_chunk
		ldr r0,=fstream
		mov r1,4+8
		mov r2,#1
		bl FStreamSeek
		ldr r0,=fstream
		ldr r1,=loop
		mov r2,#4
		bl FStreamRead
		b ignore_chunk
	process_data:
		ldr r0,[chunk_size]
		ldr r1,[current_pos]
		add r1,r1,#8
		str r1,[data_start]
		add r1,r1,r0
		str r1,[data_end]
	ignore_chunk:
		ldr r0,[chunk_size]
		ldr r1,[current_pos]
		add r1,r1,#8
		add r1,r1,r0
		str r1,[current_pos]
		ldr r0,=fstream
		bl 0x02008244
		ldr r1,[current_pos]
		sub r0,r0,#8
		cmp r1,r0
		movge r0,#0
		movlt r0,#1
		ldmia r13!,{r15}
	chunk_info:
		.word 0x0
	chunk_size:
		.word 0x0
	current_pos:
		.word 0x0
		.pool
	SetMusicInfo:
		stmdb r13!,{r4,r5,r14}
		bl 0x02079888
		ldr r1,=reload
		ldrb r0,[r1]
		cmp r0,#0
		beq no_reload
		mov r0,#0
		strb r0,[r1]
		
		ldr r1,=ofstream
		ldrb r0,[r1]
		cmp r0,#0
		beq no_close
		ldr r0,=fstream
		bl FStreamClose
	no_close:
		ldr r1,=ofstream
		mov r0,#1
		strb r0,[r1]
		ldr r0,=fstream
		bl FStreamCtor
		ldr r0,=fstream
		ldr r1,=filename
		bl FStreamFOpen
		
		mov r1,#0x0
		str r1,[loop]
		mov r1,#0xC
		str r1,[current_pos]
	header_loop:
		bl ReadWAVChunk
		cmp r0,#0
		bne header_loop
		
		mov r0,#0
		ldr r1,=start
		str r0,[r1]
		ldr r1,=timer
		str r0,[r1]
		
		mov r0, #1
		ldr r1,=playing
		strb r0,[r1]

	no_reload:
		ldr r1,=playing
		ldrb r0,[r1]
		cmp r0,#0
		beq end_set_music_info
		ldr r1,[fade_time]
		cmp r1,#0
		beq no_fade
		ldr r0,[volume]
		ldr r2,[fade_to]
		cmp r2,r0
		subge r0,r2,r0
		sublt r0,r0,r2
		bl EuclidianDivision
		ldr r1,[volume]
		ldr r2,[fade_to]
		cmp r2,r1
		addge r1,r1,r0
		sublt r1,r1,r0
		str r1,[volume]
		ldr r2,[fade_time]
		subs r2,r2,#1
		str r2,[fade_time]
		bne no_fade
		ldr r1,=playing
		ldr r0,[fade_play]
		strb r0,[r1]
	no_fade:
		mov r0,#0x8000
		ldr r1,[volume]
		mov r2,#0
		bl 0x0207CA24
		ldr r1,=0x022B7A30
		mov r3, #0x8000
		mvn r2, r3
		ldrh r0,[r1,#0x34]
		and r0,r0,r2
		strh r0,[r1,#0x34]
		ldr r4,=timer
		ldr r0,[r4]
		cmp r0,#0
		subne r0,r0,#1
		moveq r0,swap_time
		str r0,[r4]
		bne end_set_music_info
		
		sub r13,r13,#0x30
		
		ldr r2,=start
		ldr r1,[r2]
		ldr r4,[freq]
		bic r4,r4,#0x1
		mov r4,r4,lsl #0x1
		add r0,r1,r4
		str r0,[r2]
		
		ldr r0,=fstream
		ldr r2,[data_start]
		add r1,r1,r2
		mov r2,#0
		bl FStreamSeek
		
		ldr r0,[data_end]
		
		ldr r2,=start
		ldr r1,[r2]
		cmp r1,r0
		ldrgt r3,[loop]
		addgt r3,r3,r1
		subgt r3,r3,r0
		strgt r3,[r2]
		movle r5,#0
		ble read_once
		sub r2,r1,r0
		sub r5,r4,r2
		ldr r2,[data_start]
		sub r5,r5,r2
		ldr r0,=fstream
		ldr r1,=snd_addr
		mov r2,r5
		bl FStreamRead
		ldr r1,[loop]
		ldr r0,=fstream
		ldr r2,[data_start]
		add r1,r1,r2
		mov r2,#0
		bl FStreamSeek
	read_once:
		ldr r0,=fstream
		ldr r1,=snd_addr
		add r1,r1,r5
		sub r2,r4,r5
		bl FStreamRead
		
		; PNT
		ldr r0,[freq]
		mov r0,r0,lsr #0x1
		mov r1,r0,lsr #0x4
		sub r0,r0,r1
		str r0,[r13]
		; LEN
		str r1,[r13,#0x4]
		; Volume
		ldr r0,[volume]
		str r0,[r13,#0x8]
		; ???
		mov r0,#0x0
		str r0,[r13,#0xC]
		; 0x10000-TMR
		ldr r0,=0xFFB0FF ; Clock Rate
		ldr r1,[freq]
		bl EuclidianDivision
		ldr r2,[freq]
		mov r2,r2,lsr #0x1
		cmp r1,r2
		addge r0,r0,#1
		str r0,[r13,#0x10]
		; Pan
		ldr r0,=0x40
		str r0,[r13,#0x14]
		; Channel
		mov r0,#0xF
		; Sound Type
		mov r1,#1
		; Sound Start
		ldr r2,=snd_addr
		; Sound Repeat
		mov r3,#1
		bl 0x0207CA6C
		
		ldr r1,=0x022B7A30
		mov r3, #0x8000
		mvn r2, r3
		ldrh r0,[r1,#0x36]
		and r0,r0,r2
		strh r0,[r1,#0x36]
		ldrh r0,[r1,#0x32]
		orr r0,r0,r3
		strh r0,[r1,#0x32]
		
		add r13,r13,#0x30
	end_set_music_info:
		ldmia r13!,{r4,r5,r15}
		.pool
	string_strm:
		;.ascii "SOUND/BGM/bgm%04d.smd",0
		.ascii "SOUND/BGM/streamed_%d.wav",0
	.endarea
	
	; Note to Marius (EU patch translation): Usually if you see something that changes nothing
	; that means this was used as a hook at some point but has been reverted, so it's safe to remove

	.org 0x02071038
	.area 0x4
		bl SetMusicInfo
	.endarea
.close
