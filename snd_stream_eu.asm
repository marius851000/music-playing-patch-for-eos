;original by Irdkwia, EU adaptation by marius851000. I (marius) publish the **change** under the CC0 license, but there is the original copyright of Irdkwia that remain.
;Credit to Nov for research on audio synchronisation

;So... THis add double buffering.
;a buffer is for two second. They have a separation of 4 audio frame. The first half of a buffer is loaded 3 frame before the buffer switch, the second half 3 frame after switching.
.nds
.definelabel FStreamAlloc,    0x2008168     ;() Is usually done first, before any reading is done. Seems to instantiate the Filestream?
.definelabel FStreamCtor,     0x2008204     ;(r0 = PtrFStreamStruct)  Zeroes the content of the struct
.definelabel FStreamFOpen,    0x2008210     ;(r0 = PtrFStreamStruct, r1 = PtrFPath) Open the file for reading
.definelabel FStreamSeek,     0x20082A8     ;(r0 = PtrFStreamStruct, r1 = OffsetToSeekTo, r2 = unknown?(usually 0) )
;2008244h
.definelabel FStreamRead,     0x2008254     ;(r0 = PtrFStreamStruct, r1 = PtrOutBuffer, r2 = NbBytesToRead ) Read the ammount of bytes specified to the buffer, for the FStream object
.definelabel FStreamClose,    0x20082C4     ;(r0 = PtrFStreamStruct)  Close the filestream
.definelabel FStreamDealloc,  0x2008194     ;() ???
.definelabel FCheckExistance, 0x207f964     ;Seems to do some work work related to the file (maybe find where it is in the file system?). The most important stuff is that it return 1 in r0 if the file exist, 0 otherwise. r0 = a pointer to an 8bytes space to store data (stack is ok), r1 = a pointer to the file name (null terminated)
.definelabel StrLen,          0x2089a10     ;r0 = pointer to null terminated string
.definelabel RegisterAudioCallback, 0x0207cd1c ;r0 = callback id, r1 = ticks for first callbacks, r2 = ticks for other callbacks, r3 = callback pointer, sp+0 = callback arg

.definelabel buffer_base, 0x023A7080; overlay 36

.definelabel BUFFER_SIZE, 0x2b110; 2 seconds
.definelabel TIMER_BASE, 0xc1
.definelabel snd_loop, 0x1400
.definelabel BUFFER_HALF_SIZE, BUFFER_SIZE/2
.definelabel START_AUDIO_FILE_BYTES, 78 ;was 0x2C. Try to find this offset at run time, after loading a file

.open "arm9.bin", 0x02000000
	.org 0x020a72f8 ; changed
	.area 0x500 //could probably go up to 0xa00 with no problem

	playing:
		.dcb 0
	ofstream_intro:
		.dcb 0
	reload:
		.dcb 0 ; set to 1 if the stream should be reloaded
	current_file_stream:
		.dcb 0 ;0 = intro, 1 = base
	pos_in_file:
		.word 0x0 ; position in file, including header
	timer:
		.word 0x0
	filename:
		.fill 0x40, 0x0
	fstream_intro: ; file_stream
		.fill 0x48, 0x0
	fstream_base:
		.fill 0x48, 0x0
	loop:
		.word 0x0
	loop_buffer:
		.word 0x0
	;put at the end to not disrupt previous patch, which assume the position of filename
	ofstream_base:
		.dcb 0
	.align 4

	; --------------------------------
	; - return current fstream in r0 -
	; --------------------------------
	; backup the other used register
	CurrentFstreamToR0:
		stmdb r13!,{r1, lr}
		ldr r0, =current_file_stream
		ldrb r0, [r0]
		mov r1, 0x48
		mul r0, r0, r1
		ldr r1, =fstream_intro
		add r0, r0, r1
		ldmia r13!,{r1, pc}

	; --------------------------------------------------
	; - Function that load the next audio part to play -
	; --------------------------------------------------
	;r0 contain the half-buffer key of the buffer to load (0 or 1, replace next_half)
	;Will handle looping
	;Will backup utiliszed register
	PreloadNextHalfBuffer:
	;1. backup register(r0-r4)
		stmdb r13!,{r0, r1, r2, r3, r5, r6, r7, r8, r9, lr}
	;(was 3., moved here to use r0) buffer_to_fill_addr(r8) = the good address
		ldr r1, =BUFFER_HALF_SIZE
		mul r0, r0, r1
		ldr r1, =buffer_base
		add r8, r1, r0
	;file_size(r5) = file.size()
		bl CurrentFstreamToR0
		bl 0x2008244
		mov r5, r0
	;load pos_in_file(r6)
		ldr r6, [pos_in_file]
	;if pos_in_file == file.length { pos_in_file = START_AUDIO_FILE_BYTES }
		cmp r6, r5
		moveq r6, START_AUDIO_FILE_BYTES
	;2. if not pos_in_file(r6) + BUFFER_HALF_SIZE >= file_size(r5) {
		ldr r1, =BUFFER_HALF_SIZE
		add r0, r6, r1
		cmp r0, r5
		bge preload_next_half_buffer_overflow_part1
		;bytes_to_read(r7) = BUFFER_HALF_SIZE;
			ldr r7, =BUFFER_HALF_SIZE
			b preload_next_half_buffer_overflow_part1_end

		; } else {
		preload_next_half_buffer_overflow_part1:
		;bytes_to_read(r7) = file_size(r5) - pos_in_file(r6);
			sub r7, r5, r6
	;end
		preload_next_half_buffer_overflow_part1_end:
	;bytes_read(r9) = 0
		mov r9, #0
		preload_next_half_buffer_inner_jump:
	;4. file.seek(pos_in_file(r6));
		bl CurrentFstreamToR0
		mov r1, r6
		mov r2, #0
		bl FStreamSeek
	;5. file.read(buffer_to_fill_addr, bytes_to_read)
		bl CurrentFstreamToR0
		mov r1, r8
		mov r2, r7
		bl FStreamRead
	;bytes_read(r9) += bytes_to_read(r7)
		add r9, r9, r7
	;pos_in_file(r6) += bytes_to_read(r7)
		add r6, r6, r7
	;persist pos_in_file(r6)
		str r6, [pos_in_file]
	
	;6. if (bytes_read(r9) < BUFFER_HALF_SIZE) {
		ldr r1, =BUFFER_HALF_SIZE
		cmp r9, r1
		bge preload_next_half_buffer_overflow_part2_end
		; pos_in_file = START_AUDIO_FILE_BYTES
			mov r6, START_AUDIO_FILE_BYTES
		; buffer_to_fill_addr(r8) += nb_bytes_to_read(r7)
			add r8, r8, r7
		; nb_bytes_to_read(r1) = BUFFER_SIZE - nb_bytes_to_read(r7)
			ldr r1, =BUFFER_HALF_SIZE
			sub r7, r1, r7
		; if (current_file_stream == 0) { current_file_stream = 1;}
		ldr r0, =current_file_stream
		ldrb r1, [r0]
		cmp r1, #1
		beq preload_next_half_buffer_loop_no_file_change
		mov r1, #1
		strb r1, [r0]
		preload_next_half_buffer_loop_no_file_change:

		; jump @load_audio_inner_jump
			b preload_next_half_buffer_inner_jump
	preload_next_half_buffer_overflow_part2_end:
	; restore registers and return
	ldmia r13!,{r1, r2, r3, r4, r5, r6, r7, r8, r9, pc}




	; ----------------
	; - Some IO init -
	; ----------------
	SetMusicInfo:
		stmdb r13!,{r4,r5,r6,r7,r8,r9,r10,r14} ;r9 and r10 may be used as scrash register which stay here after call to functions
		; the original function that this one replace
		;TODO: maybe put it at the end of the function for better timing. Will need tests.
		bl 0x02079c20 ; changed
		
		; if !reload { jump @no_reload; } else ...
		ldr r1,=reload
		ldrb r0,[r1]
		cmp r0,#0
		beq no_reload

		sub sp, sp, 4
		;;;;;;;;;;;;
		;;; Test ;;;
		;;;;;;;;;;;;
		; only 0, 2 and 4 seems to be used by the game
		mov r0, 7
		ldr r1, =1047311 //ended with a 311 before
		mov r2, r1
		ldr r3, =restart_buffer
		; Don’t care about arg1

		bl RegisterAudioCallback

		;idek
		mov        r0,#0x0000
    	mov        r1,#0
        mov        r2,#0x80
		mov        r3,#0
        bl         0x0207cc50

		add sp, sp, 4
		ldr r1,=reload

		; reload = false
		mov r0,#0
		strb r0,[r1]
		
		; if ofstream_base { FileClose(&fstream_base); ofstream_base = true; }
		ldr r1,=ofstream_base
		ldrb r0,[r1]
		cmp r0,#0
		beq no_close_base
		ldr r0,=fstream_base
		bl FStreamClose
		ldr r0, =ofstream_base
		mov r1, #1
		strb r1, [r0]
	no_close_base:
		; if ofstream_intro { FileClose(&fstream_intro); ofstream_intro = false; }
		ldr r1,=ofstream_intro
		ldrb r0,[r1]
		cmp r0,#0
		beq no_close_intro
		ldr r0,=fstream_intro
		bl FStreamClose
		ldr r0, =ofstream_intro
		mov r1, #1
		strb r1, [r0]
	no_close_intro:
		; FileInitVeneer(&fstream_base)
		ldr r0,=fstream_base
		bl FStreamCtor

		; FileOpen(&fstream_base, &filename)
		ldr r0,=fstream_base
		ldr r1,=filename
		bl FStreamFOpen
		
		; current_file_stream = 1
		ldr r0, =current_file_stream
		mov r1, #1
		strb r1, [r0]

		; mutate the file name, replace last char with s
		ldr r0, =filename
		bl StrLen
		ldr r1, =filename
		add r9, r0, r1
		sub r9, r9, 1
		mov r0, 's'
		ldrb r10, [r9]
		strb r0, [r9]
		sub sp, sp, #8
		mov r0, sp
		bl FCheckExistance
		add sp, sp, #8
		cmp r0, #0
		beq no_intro_file

		; FileOpen(&fstream_intro, &filename)
		ldr r0,=fstream_intro
		ldr r1,=filename
		bl FStreamFOpen

		; current_file_stream = 0
		ldr r0, =current_file_stream
		mov r1, #0
		strb r1, [r0]
	no_intro_file:
		; restore the string
		strb r10, [r9]
		
		; pos_in_file = START_AUDIO_FILE_BYTES
		mov r0,START_AUDIO_FILE_BYTES ; skip header
		ldr r1,=pos_in_file
		str r0,[r1]

		; timer = 1
		mov r0, #1
		ldr r1,=timer
		str r0,[r1]
		
		; playing = true
		mov r0, #1
		ldr r1,=playing
		strb r0,[r1]

		;load first half of the audio buffer
		mov r0, #0
		bl PreloadNextHalfBuffer

	no_reload:
		; if (!playing) { return }
		ldr r1,=playing
		ldrb r0,[r1]
		cmp r0,#0
		beq end_of_main_function


		; TODO: understand this !!!
		; GLOBAL_UNK->field_0x34 &= 0xFFEF; (short)
		ldr r1,=0x022b8370 ; changed ; unknown value, contain some struct. Called GLOBAL_UNK. Semmingly related to arm7 FIFO's
		mov r3, #0x10
		mvn r2, r3
		ldrh r0,[r1,#0x34]
		and r0,r0,r2
		strh r0,[r1,#0x34]
		
		; timer -= 1;
		ldr r4, =timer
		ldr r0, [r4]
		sub r0, r0, #1
		str r0, [r4]

		;;; the big jump
		; if at 1/4th, preload 2nd half
		cmp r0, TIMER_BASE-(TIMER_BASE/4)
		moveq r0, 1
		bleq PreloadNextHalfBuffer
		beq end_of_main_function
		; if at 3/4th, preload 1st half for next restart
		cmp r0, TIMER_BASE/4
		moveq r0, 0
		bleq PreloadNextHalfBuffer
		beq end_of_main_function
		; if at 0... Do nothing. Resetting the timer could result in a race condition

	
	end_of_main_function:
		ldmia r13!,{r4,r5,r6,r7,r8,r9,r10,r15}
	
	restart_buffer:
		push lr

		; rought plan
		; PlayBuffer(buffer_addresses[next_buffer], ...);
		; next_buffer = next_buffer == 0 : 1 ? 0;
		; timer = TIMER_BASE

		; r4 = buffer_base
		ldr r4, =buffer_base

		sub r13,r13,#0x30 ; allocate 0x30 of stack
		
		;;; Set various variable and call the function to tell the arm7 core to play it
		; PNT
		ldr r0,=(BUFFER_SIZE-snd_loop)/4
		;ldr r0, =(BUFFER_SIZE/4-1)
		str r0,[r13]
		; LEN
		ldr r0,=snd_loop/4
		;mov r0, #1
		str r0,[r13,#0x4]
		; Volume
		ldr r0,=0x3C
		str r0,[r13,#0x8]
		; ???
		mov r0,#0x0
		str r0,[r13,#0xC]
		; 0x10000-TMR
		ldr r0,=0x17C ; timer stuff. I think
		str r0,[r13,#0x10]
		; Pan
		ldr r0,=0x40
		str r0,[r13,#0x14]
		; Channel
		mov r0,#4
		; Sound Type
		mov r1,#1
		; Sound Start
		mov r2, r4
		; Sound Repeat
		mov r3,#1
		bl 0x0207ce04 ; changed ; tell arm7 to play an audio

		; timer = swap_time
		ldr r1, =timer
		mov r0, TIMER_BASE
		str r0, [r1]

		

		; ----------------------------------
		; - some de-initilization stuff... -
		; ----------------------------------

		; GLOBAL_UNK->field_0x36 &= 0xFFEF // short
		ldr r1,=0x022b8370 ; changed ; //GLOBAL_UNK
		mov r3, #0x10
		mvn r2, r3 ; 0xFFFFFEFF
		ldrh r0,[r1,#0x36]
		and r0,r0,r2
		strh r0,[r1,#0x36]

		; GLOBAL_UNK->field_0x32 &= 0x0010
		ldrh r0,[r1,#0x32]
		orr r0,r0,r3
		strh r0,[r1,#0x32]
		
		add r13,r13,#0x30 ; free the previously allocated stack
		pop pc
	.pool
	


	test_string:
	.asciiz "SOUND/BGM/bgm.swd"
	.endarea

	.org 0x020711dc ; changed
	;hmmm... That seems useless, this address already point here. Will still change just in case.
	.area 0x4
		bl 0x0206ce64 ; changed
	.endarea

	.org 0x020713d0 ; changed
	.area 0x4
		bl SetMusicInfo
	.endarea
.close
